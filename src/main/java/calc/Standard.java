package calc;

public class Standard {
    public static double memory;

    public static double add(double a, double b) {
        return a + b;
    }

    public static double sub(double a, double b) {

        return a - b;
    }

    public static double mul(double a, double b) {

        return a * b;
    }

    public static double div(double a, double b) {

        return a / b;
    }

    public static int mod(double a, double b) {

        return (int) (a % b);
    }

    public static double clearMemory() {
        return memory = 0;
    }
}


