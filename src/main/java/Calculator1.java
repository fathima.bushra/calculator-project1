
import calc.Scientific;
import calc.Standard;
import java.util.Scanner;

    public class Calculator1 {
        public static void main(String[] args) {
            //System.out.println("Hello");
            Scanner in = new Scanner(System.in);
            System.out.println("1. To open Standard Calculator type std  ");
            System.out.println("2. To open scientific calculator type sci ");
            char mode = in.next().charAt(0);
            if (mode == '1') {
                System.out.println("Enter the operand values: ");
                Double a = in.nextDouble();
                Double b = in.nextDouble();
                System.out.println("enter the operator");
                String ch = in.next();


                switch (ch) {
                    case "+":
                        double add1 = Standard.add(a, b);
                        System.out.println("Addition:" + add1);
                        break;
                    case "-":
                        double sub1 = Standard.sub(a, b);
                        System.out.println("Subtraction:" + sub1);
                        break;

                    case "*":
                        double mul1 = Standard.mul(a, b);
                        System.out.println("Multiplication:" + mul1);
                        break;


                    case "/":
                        double div1 = Standard.div(a, b);
                        System.out.println("Division:" + div1);
                        break;
                    case "%":
                        if (a > b) {
                            int mod1 = Standard.mod(a, b);
                            System.out.println("Modulus:" + mod1);
                            break;
                        } else {
                            int mod1 = Standard.mod(b, a);
                            System.out.println("Modulus:" + mod1);
                            break;
                        }

                    case "c":
                        int mem = (int) Standard.clearMemory();
                        System.out.println("clear memory" + mem);
                        break;


                    default:
                        throw new IllegalStateException("Invalid Operation: " + ch);
                }

            } else if (mode == '2') {
                //double operator=0.0d;

                System.out.println("Enter the operator: ");
                String ch = in.next();
                if ((ch.equals("+")) || (ch.equals("-")) || (ch.equals("/")) || (ch.equals("*")) || (ch.equals("%")) || (ch.equals("pow")) || (ch.equals("c"))) {
                    System.out.println("enter the operand");
                    Double a = in.nextDouble();
                    Double b = in.nextDouble();


                    switch (ch) {
                        case "+":
                            double add1 = Scientific.add(a, b);
                            Scientific.setMemory(add1);
                            System.out.println(Scientific.getMemory());
                            System.out.println("Addition:" + add1);
                            break;
                        case "-":
                            double sub1 = Scientific.sub(a, b);
                            Scientific.setMemory(sub1);
                            System.out.println(Scientific.getMemory());
                            System.out.println("Subtraction:" + sub1);
                            break;

                        case "*":
                            double mul1 = Scientific.mul(a, b);
                            Scientific.setMemory(mul1);
                            System.out.println(Scientific.getMemory());
                            System.out.println("Multiplication:" + mul1);
                            break;


                        case "/":
                            double div1 = Scientific.div(a, b);
                            Scientific.setMemory(div1);
                            System.out.println(Scientific.getMemory());
                            System.out.println("Division:" + div1);
                            break;
                        case "pow":

                            double p = Scientific.pow(a, b);
                            Scientific.setMemory(p);
                            System.out.println(Scientific.getMemory());
                            System.out.println("Pow:" + p);
                            break;
                        case "%":
                            if (a > b) {
                                int mod1 = Standard.mod(a, b);
                                Scientific.setMemory(mod1);
                                System.out.println(Scientific.getMemory());
                                System.out.println("Modulus:" + mod1);
                                break;

                            } else {
                                int mod1 = Standard.mod(b, a);
                                Scientific.setMemory(mod1);
                                System.out.println(Scientific.getMemory());
                                System.out.println("Modulus:" + mod1);
                                break;
                            }
                        case "c":
                            int mem = (int) Scientific.clearMemory();
                            System.out.println("clear memory" + mem);
                            break;

                        default:
                            throw new IllegalStateException("Invalid Operation: " + ch);

                    }
                }





            else  if (ch.equals("sin") || ch.equals("cos") || ch.equals("tan") || ch.equals("sqrt"))
            {
                    System.out.println("enter the value");
                    Double a = in.nextDouble();
                    switch (ch) {


                        case "sin":
                            double sin1 = Scientific.sin(a);
                            Scientific.setMemory(sin1);
                            System.out.println(Scientific.getMemory());
                            System.out.println("Sin:" + a + " is" + sin1);
                            break;
                        case "cos":
                            double cos1 = Scientific.cos(a);
                            Scientific.setMemory(cos1);
                            System.out.println(Scientific.getMemory());
                            System.out.println("cos of " + a + " is" + cos1);
                            break;
                        case "tan":
                            double tan1 = Scientific.tan(a);
                            Scientific.setMemory(tan1);
                            System.out.println(Scientific.getMemory());
                            System.out.println("tan of " + a + " is" + tan1);
                            break;


                        case "sqrt":
                            double sq = Scientific.sqrt(a);
                            Scientific.setMemory(sq);
                            System.out.println(Scientific.getMemory());
                            System.out.println("sqrt:" + a + " " + sq);
                            break;


                    }


                } else {

                    System.out.println("Invalid Operation: " + ch);

                }
            }
        }
    }









































































